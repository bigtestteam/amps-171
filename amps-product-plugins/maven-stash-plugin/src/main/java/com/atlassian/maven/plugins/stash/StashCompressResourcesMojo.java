package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.CompressResourcesMojo;

import org.apache.maven.plugins.annotations.Mojo;

/**
 * @since 3.10
 */
@Mojo(name = "compress-resources")
public class StashCompressResourcesMojo extends CompressResourcesMojo
{
}
